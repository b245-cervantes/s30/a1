//use the count operator to count total number of fruits on sale.

db.fruits.aggregate([
	{
		$match:{onSale: true
	}
},
    {
    	$count: "fruitOnSale"
    }
]);

//Use the count operator to cunt the total number of fruits with stock more than or equal to 20
db.fruits.aggregate([
	{
		$match:{onSale: true, {$gte:20}}
},
    {
    	$count: "enoughStock"
    }

]);

//Use the average operator to get the average price of fruits on sale per supploer

db.fruits.aggregate([
	{
		$match:{onSale: true}
},
    {
    	$group:{
    		_id:"$supplier_id" , avg_price: {$avg:"$price"}
    	}
    },{
    	$sort:
    	{
    		avg_price:-1
    	}
    }

]);

//Use the max operator to get the highest price of a fruit per supplier

db.fruits.aggregate([
	{
		$match:{onSale: true}
},
    {
    	$group:{
    		_id:"$supplier_id" , max_price: {$max:"$price"}
    	}
    },{
    	$sort:
    	{
    		max_price:1
    	}
    }

]);

//lowest price

db.fruits.aggregate([
	{
		$match:{onSale: true}
},
    {
    	$group:{
    		_id:"$supplier_id" , min_price: {$min:"$price"}
    	}
    },{
    	$sort:
    	{
    		min_price:1
    	}
    }

]);